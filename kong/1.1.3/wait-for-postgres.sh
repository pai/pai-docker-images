#!/bin/bash
# wait-for-postgres.sh

set -e

host="$1"
shift
cmd="$@"

export PGPASSWORD='kong';

until psql -h "$host" -U "kong" -c '\l'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - executing migrations"

#kong migrations up -vv
kong migrations bootstrap -vv

>&2 echo "Migration ran - executing command"

exec $cmd
