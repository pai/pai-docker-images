return {
  no_consumer = true,
  fields = {
    inject_consumer_custom_id = { type = "boolean", default = true },
    logging_enabled = { type = "boolean", default = true },
    debug_logging_enabled = { type = "boolean", default = false },
  }
}
