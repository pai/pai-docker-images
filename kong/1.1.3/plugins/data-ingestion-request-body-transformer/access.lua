local multipart = require "multipart"
local cjson = require "cjson"
local constants = require "kong.constants"

local req_get_headers = ngx.req.get_headers
local req_set_header = ngx.req.set_header
local req_read_body = ngx.req.read_body
local req_set_body_data = ngx.req.set_body_data
local req_get_body_data = ngx.req.get_body_data
local type = type
local string_find = string.find
local pcall = pcall

local _M = {}

local CONTENT_LENGTH = "content-length"
local CONTENT_TYPE = "content-type"
local HOST = "host"
local JSON, MULTI, ENCODED = "json", "multi_part", "form_encoded"
local SSO_FIELD_NAME = "sso_id"

local function log(...)
  ngx.log(ngx.ERR, ...)
end

local function parse_json(body)
  if body then
    local status, res = pcall(cjson.decode, body)
    if status then
      return res
    end
  end
end

local function get_content_type(content_type)
  if content_type == nil then
    return
  end
  if string_find(content_type:lower(), "application/json", nil, true) then
    return JSON
  elseif string_find(content_type:lower(), "multipart/form-data", nil, true) then
    return MULTI
  elseif string_find(content_type:lower(), "application/x-www-form-urlencoded", nil, true) then
    return ENCODED
  end
end

local function transform_json_body(consumer, body, content_length)
  local appended = true
  local content_length = (body and #body) or 0
  local parameters = parse_json(body)

  if parameters == nil and content_length > 0 then
    return false, nil
  end

  parameters[SSO_FIELD_NAME] = consumer
  appended = true

  if appended then
    return true, cjson.encode(parameters)
  end
end


local function transform_body(conf)
  local content_type_value = req_get_headers()[CONTENT_TYPE]
  local content_type = get_content_type(content_type_value)

  local injected_header = constants.HEADERS.CONSUMER_ID
  if conf.inject_consumer_custom_id then
    injected_header = constants.HEADERS.CONSUMER_CUSTOM_ID
  end

  local consumer = req_get_headers()[injected_header]
  
  -- log("Consumer ", consumer)
  
  if content_type == nil or consumer == nil then
    return
  end

  -- Call req_read_body to read the request body first
  req_read_body()
  local body = req_get_body_data()
  local is_body_transformed = false
  local content_length = (body and #body) or 0

  if content_type == JSON then
    is_body_transformed, body = transform_json_body(consumer, body, content_length)
  end
  
  -- log("Body transformed ", is_body_transformed, #body, body)

  if is_body_transformed then
    req_set_body_data(body)
    req_set_header(CONTENT_LENGTH, #body)
  end
end

function _M.execute(conf)
  transform_body(conf)
end

return _M
