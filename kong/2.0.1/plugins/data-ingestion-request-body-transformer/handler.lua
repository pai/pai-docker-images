local BasePlugin = require "kong.plugins.base_plugin"
local access = require "kong.plugins.data-ingestion-request-body-transformer.access"

local DataIngestionRequestBodyTransformerHandler = BasePlugin:extend()

function DataIngestionRequestBodyTransformerHandler:new()
  DataIngestionRequestBodyTransformerHandler.super.new(self, "data-ingestion-request-body-transformer")
end

function DataIngestionRequestBodyTransformerHandler:access(conf)
  DataIngestionRequestBodyTransformerHandler.super.access(self)
  access.execute(conf)
end

DataIngestionRequestBodyTransformerHandler.PRIORITY = 802
DataIngestionRequestBodyTransformerHandler.VERSION = "0.1.0"

return DataIngestionRequestBodyTransformerHandler
