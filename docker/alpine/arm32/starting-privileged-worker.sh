#!/bin/sh

CONTAINER_NAME=privileged_worker
NETWORK_NAME=network


echo "Getting latest image"
docker pull 127.0.0.1:5000/privileged-worker


# https://docs.docker.com/engine/reference/commandline/wait/
docker stop $CONTAINER_NAME
docker container wait $CONTAINER_NAME 
docker rm $CONTAINER_NAME


docker run -d --name $CONTAINER_NAME --privileged 127.0.0.1:5000/privileged-worker

until [ "/usr/bin/docker inspect -f {{.State.Running}} $CONTAINER_NAME"=="true" ]; do
    echo "Waiting container";
    sleep 1;
done;

echo "container started! Connecting $CONTAINER_NAME to $NETWORK_NAME network.."
docker network connect $NETWORK_NAME $CONTAINER_NAME

echo "Connected"

docker container inspect $CONTAINER_NAME

exec docker logs -f $CONTAINER_NAME
